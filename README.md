# Development and characterisation of a telemetry infrastructure to study environmental factors in a custom built aquaponics system

Here we present the data treatment, exploration, analysis and figure creation for the aquaponics system project published in Journal of Engineering Science and Technology Review [here](http://jestr.org/downloads/Volume17Issue1/fulltext241712024.pdf).

doi:10.25103/jestr.171.24

## Analysis

### Data prep and exploration

This Jupyter notebook shows how the data was processed, filtered and explored to gather insights from the measurements. At the bottom the code to create Figure 8 of the publication is presented.

### PCA analysis

This Jupyter notebook contains the code for the PCA analysis performed for Figure 9 and a few extra plots.

### Regression

This Jupyter notebook shows the univariate linear regression analysis of room temperature against all other variables for Figure 10 and 11.

## Installation

1. Install conda
2. Create conda environment using:

```bash
conda env create -f env.yml
```

3. Enter the script folder and start up a Jupyter instance:

```bash
jupyter lab
```
